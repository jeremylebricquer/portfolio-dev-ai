import { Projects } from "./Projects";

export interface ProjectDetailProps {
    selectedProject: Projects | null;
    isModalOpen: boolean;
    closeModal: () => void;

}