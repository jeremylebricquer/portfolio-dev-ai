import { ReactNode } from "react";

export interface ModalProps {
    isOpen: boolean;
    closeModal: () => void;
    title: string | undefined;
    desc: string | undefined;
    children: ReactNode;
}