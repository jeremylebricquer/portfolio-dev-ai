export interface Projects {
    id: number,
    title: string,
    description: string,
    tags: string[]
    link: string,
    image: string,
    type: string

}