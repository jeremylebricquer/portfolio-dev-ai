import { LinkData } from "./links";

export interface NavbarProps {
    links: LinkData[];
    openModal: (link: LinkData) => void;
}