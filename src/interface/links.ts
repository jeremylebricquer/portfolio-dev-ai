export interface LinkData {
    id: number;
    text: string;
    component: React.ReactNode;
    sectionTitle: string,
    sectionDesc: string,
}