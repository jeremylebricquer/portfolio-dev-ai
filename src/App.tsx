import { useState } from 'react';
import './App.css'
import { Modal } from './components/Modal'
import { Navbar } from './components/Navbar'
import { Home } from './view/Home'
import { LinkData } from './interface/links';
import { links } from './data/links';
import backgroundImage from './assets/img/bg.webp';


const App = () => {

  const [showModal, setShowModal] = useState(false);
  const [selectedLink, setSelectedLink] = useState<LinkData | null>(null);


  const openModal = (link: LinkData) => {
    setSelectedLink(link);
    setShowModal(true)

  }

  const closeModal = () => {
    setShowModal(false)
  }


  return (
    <div className="flex flex-col items-center h-screen bg-[url]" style={{ backgroundImage: `url(${backgroundImage})`, backgroundSize: 'cover', backgroundPosition: 'center center', backgroundRepeat: 'no-repeat' }}>
      <div className="absolute inset-0 bg-neutral-500/20"></div>
      <Navbar openModal={openModal} links={links} />
      <Modal isOpen={showModal} closeModal={closeModal} title={selectedLink?.sectionTitle} desc={selectedLink?.sectionDesc}> {selectedLink && selectedLink.component} </Modal>
      <div className="flex flex-col items-center flex-grow py-16">
        <Home />
      </div>
    </div>
  )
}

export default App
