export type resumeType = {
    Education: { [key: string]: string[] }[];
    Experience: { [key: string]: string[] }[];
}

export const resumeInfo: resumeType = {
    Education: [
        {
            "Artificial Intelligence Developer":
                ["SIMPLON.CO - CCI Lannion - 10/2021 - 05/2023",
                    "After my degree in web development,\
                    I already knew that I wanted to specialize in data.\
                    I joined the Simplon training.This allowed me to acquire knowledge in datascience, dataviz, neural networks, image processing and Python."]
        },
        {
            "Web / mobile developer":
                ["CALLAC SOFT COLLEGE - Callac - 04/2019 - 03/2020",
                    "Beginning of my journey in development and my retraining.\
                    After a successful internship in the industry to see if it suited me, \
                    I joined this training to acquire the fundamentals of algorithms and the web.\
                    C , java , html/css/js , php , native mobile , react , laravel "]
        },
    ],
    Experience: [
        {
            "Frontend developer":
                ["Open - 3 month - Lannion / 2023",
                    "Mission as frontend developer at Orange Innnovation.\
                    Work on the Orange Money project.\
                    Orange Money is the Orange group's money transfer and mobile payment service,\
                    offered in the majority of African countries.\
                    Resolved bug tickets described by the qualification team.\
                    Development of user stories in accordance with the design specification. Component refactoring to make them more generic",
                    "Technical stack : Angular, Typecript, Mongo,\
                    Kanban , Jira , Gitlab "]
        },
        {
            "AI Developer":
                ["Orange Innovation -1 year - Lannion / 2022-2023",
                    "My work-study contract in the R&D field of Orange for 4.0 Industry.\
                This period was divided into three main part : \
                State of the art of federated learning. \
                Train and evaluate state of the art computer vision models on a custom dataset and deploy them on embedded electronics(Jetson,Depth camera) with quantized models(MobileNetv2 - SSD, Python, Docker,Yolov5, Opencv, Pytorch).\
                Federated learning with Flower for multi-label image classification on small devices.\
                I built a complete client-server ML (micro-service) architecture. Using 2 Nuc to simulate my remote clients and my computer for the server. \
                Customers locally trained their models with a private dataset and then aggregated the model parameters on a remote server. Monitoring and evaluation with MLFlow, quantified and customized CNN Pytorch model to increase speed on embedded devices. \
                Everything is dockerized. An API was also implemented with FastAPI for the dialog between the Flower backend and a web interface that allows to launch services and perform inference.",
                    "Technical stack : Pytorch, MLFlow, Docker, Flower.",
                    "https://gitlab.com/jeremylebricquer/federatedlearning"]
        },
        {
            "Web developer":
                ["Internship - 3 months - Orange Lannion - 2019-2020",
                    "Internship within an Orange web development team for an internal solution.\
                    My internship was divied into two main part\
                    Self-training on the docker environment , Angular and Typescript.\
                    Development of an administration portal .Installation of an HMI",
                    "Technical stack : Angular,Typescript, Docker, API REST."]

        }
    ],
};
