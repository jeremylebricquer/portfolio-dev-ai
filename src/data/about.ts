function calculateAge(dateOfBirth: Date): number {
    const today = new Date();
    const diffInMilliseconds = today.getTime() - dateOfBirth.getTime();
    const ageDate = new Date(diffInMilliseconds);
    const age = Math.abs(ageDate.getUTCFullYear() - 1970);
    return age;
}

const birthDate = new Date(1989, 11, 23);
const age = calculateAge(birthDate);

export const about = {

    title: "My name is Jérémy Le bricquer, I'm a IA developer",
    desc: "Currently I am now looking for a job as a data/IA developer, python. \
        My career in IT is the result of a retraining after 4 years in the French Navy.\
        I first turned to computer training focused on the web where I was able to acquire the basics of development.\
        I decided to continue towards AI data with the Simplon data/IA developer training.\
        My passion for computer science leads me to follow courses such as Neural Networks, Computer Vision and Data Science.",
    name: "Jérémy Le bricquer",
    email: "jeremylebricquer@gmail.com",
    age: age.toString(),
    from: "Lannion",
    linkedin: "https://www.linkedin.com/in/jeremy-lebricquer/",
    gitlab: "https://gitlab.com/jeremylebricquer"

}