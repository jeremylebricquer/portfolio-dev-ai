type SkillList = {
    type: string,
    skills: string,
    percent: string
};

type softSkillList = {
    type: string,
    percent: string
};

export const skillList: SkillList[] = [
    {
        type: "AI",
        skills: 'Computer Vision Models, Federated Learning',
        percent: "80%",
    },
    {
        type: "Python",
        skills: "Pytorch, Tensorflow, FastAPI, Opencv, Numpy, Pandas",
        percent: "80%",
    },
    {
        type: "Devops",
        skills: "Docker, MLFlow, Github, Gitlab",
        percent: "75%",
    },
    {
        type: "Web",
        skills: "Angular, React, Tailwindcss, Firebase, Laravel",
        percent: "80%",
    },
    {
        type: "Database",
        skills: "MongoDB, MySql, Postgres,SqLite",
        percent: "80%",
    }, {
        type: "Other",
        skills: "Bash, C",
        percent: "65%",
    }
]


export const softSkillList: softSkillList[] = [

    {
        type: "Analytical mind",
        percent: "80%",
    },
    {
        type: "Communication",
        percent: "75%",
    },
    {
        type: "Problem-solving",
        percent: "85%",
    },
    {
        type: "Collaboration",
        percent: "80%",
    },
    {
        type: "Creativity",
        percent: "80%",
    }, {
        type: "Desire to learn , Curiosity",
        percent: "100%",
    }
]