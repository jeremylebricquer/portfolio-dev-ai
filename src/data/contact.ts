type contactType = {
  name: string,
  location: string,
  phoneNumber: string,
  email: string,
  infoDescription: string[]
}

export const contactInfo: contactType = {
  name: "Jérémy Le bricquer",
  location: "Lannion , Bretagne , France",
  phoneNumber: "+33 6 13 26 05 66",
  email: "jeremylebricquer@gmail.com",
  infoDescription: ["I'm looking for a job in data science and AI.",
    "Also open for python dev and web/mobile", "Feel free to contact me!"]
};
