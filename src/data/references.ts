export type ReferenceList =
    {
        title: string
        text: string
    }


export const referenceList: ReferenceList[] = [
    {
        title: "Supervisor at Orange Innovation",
        text: "Excellente autonomie.\
    Bonne prise d'initiative qui viennent contrebalancer ses petites lacunes(qu'il corrige grâce à l'expérience qu'il acquiert).\
    Jérémy arrive facilement à « rentrer » dans le code et à développer de nouvelles fonctionnalités.\
    Il a travailler sur la création de modèles d'intelligence artificielle(Tensorflow mais surtout PyTorch) et l'apprentissage fédéré avec MLFlow et FlowerFL.\
    Bon esprit de synthèse et de rédaction."
    }
]