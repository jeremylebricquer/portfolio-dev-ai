import { Projects } from "../interface/Projects";
import Fl from '../assets/img/Projects/FL.webp'
import dataChall from '../assets/img/Projects/data_challenge.webp'
import recoChiffre from '../assets/img/Projects/chiffre.webp'
import sentiment from '../assets/img/Projects/sentiment_analysis.webp'
import meteo from '../assets/img/Projects/cloudy7.webp'
import bonheur from '../assets/img/Projects/bonheur_monde.webp'
import creche from '../assets/img/Projects/creche2.webp'
import sudoku from '../assets/img/Projects/Sudoku.webp'


export const projects: Projects[] = [
    {
        id: 1,
        title: 'Federated Learning',
        description: 'Federated  Learning for 4.0 industry',
        tags: ["#Python ", "#PyTorch ", "#Flower ", "#Mlflow", "#PostgreSQL ", "Docker ", "#Bash "],
        link: "https://gitlab.com/jeremylebricquer/federatedlearning",
        image: Fl,
        type: 'AI'

    },
    {
        id: 2,
        title: 'Data challenges',
        description: "Prédire à court terme le taux d’occupation à bord des trains en temps",
        tags: ["#Python", "#Sklearn"],
        link: "https://gitlab.com/jeremylebricquer/data-challenge-sncf",
        image: dataChall,
        type: 'Data science'

    },
    {
        id: 3,
        title: 'Reconnaissance chiffre',
        description: "reconnaitre un chiffre dessiné à la souris par n'importe quel utilisateur.",
        tags: ["#Python", "#Tkinter", "#PIL", "#Tensorflow"],
        link: "https://gitlab.com/jeremylebricquer/initiation_reseaux_neuronaux",
        image: recoChiffre,
        type: 'AI'

    },
    {
        id: 4,
        title: 'Sentiment analysis',
        description: 'NLP',
        tags: ["#Pandas", "#Numpy", "#Sklearn", "#Nltk", "#Word2vec"],
        link: "https://gitlab.com/jeremylebricquer/sentiment_analysis",
        image: sentiment,
        type: 'Machine Learning'

    },
    {
        id: 5,
        title: "Détecter automatiquement la météo",
        description: "vérifier s'il est possible de détecter et classer de manière non supervisée la catégorie météo d'un jeu d'images",
        tags: ["#Pandas", "#Numpy", "#Sklearn", "#Seaborn", "#Ploty", "#MLPClassifier"],
        link: "https://gitlab.com/jeremylebricquer/detecter-automatiquement-la-meteo",
        image: meteo,
        type: 'Machine Learning'

    },
    {
        id: 6,
        title: 'Rapport sur le bonheur dans le monde',
        description: "Algorithme d'apprentissage automatique non supervisé pour regrouper les pays en fonction de caractéristiques telles que la production économique, le soutien social, l'espérance de vie, la liberté, l'absence de corruption et la générosité.\
        Le Rapport sur le bonheur dans le monde détermine l'état du bonheur mondial.",
        tags: ["#Pandas", "#Numpy", "#Sklearn", "#Seaborn", "#Ploty"],
        link: "https://gitlab.com/jeremylebricquer/rapport-sur-le-bonheur-dans-le-monde",
        image: bonheur,
        type: 'Data science'
    },
    {
        id: 7,
        title: 'Creche app',
        description: 'Desktop app à destination de structure de la petite enfance',
        tags: ["#Electron", "#React", "#Php", "#Typescript"],
        link: "https://gitlab.com/jeremylebricquer/creche__app",
        image: creche,
        type: "Web"
    },
    {
        id: 8,
        title: 'AI Sudoku',
        description: "Solveur de sudoku par webcam",
        tags: ["#Python", "#OCR", "#Tensorflow"],
        link: "#",
        image: sudoku,
        type: 'AI'

    },

];