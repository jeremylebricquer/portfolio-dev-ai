import { LinkData } from "../interface/links";
import { About } from "../view/About";
import { Contact } from "../view/Contact";
import { Portfolio } from "../view/Portfolio";
import { Resume } from "../view/Resume";


export const links: LinkData[] = [
    {
        id: 1,
        text: 'About',
        component: <About />,
        sectionTitle: "Get to know me",
        sectionDesc: "About me",
    },
    {
        id: 2,
        text: 'Resume',
        component: <Resume />,
        sectionTitle: "Check out my Resume",
        sectionDesc: "Resume",
    },
    {
        id: 3,
        text: 'Portfolio',
        component: <Portfolio />,
        sectionTitle: "Portfolio",
        sectionDesc: "Some of my projects !",
    },
    {
        id: 4,
        text: 'Contact',
        component: <Contact />,
        sectionTitle: "Get in Touch",
        sectionDesc: "Feel free to contact me anytime",
    }
];
