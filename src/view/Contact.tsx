import { contactInfo } from "../data/contact"

export const Contact = () => {


    return (
        <div className="flex flex-row lg:justify-center">
            <div className="flex flex-col">
                <h4 className="mb-3 text-2xl font-medium text-white">Contact Info</h4>
                <p className="mb-6 font-normal">
                    {contactInfo.infoDescription[0]}
                    <br />
                    {contactInfo.infoDescription[1]}
                    <br />
                    {contactInfo.infoDescription[2]}
                </p>
                <ul className="list-unstyled list-info">
                    <li className="flex items-center">
                        <div className="pr-5">
                            <svg xmlns="http://www.w3.org/2000/svg" width="32" height="32" fill="currentColor" className="bi bi-person-fill" viewBox="0 0 16 16"> <path d="M3 14s-1 0-1-1 1-4 6-4 6 3 6 4-1 1-1 1H3zm5-6a3 3 0 1 0 0-6 3 3 0 0 0 0 6z" /> </svg>
                        </div>
                        <div className="py-3 pr-6 pl-6 border-l-2 border-white  border-opacity-20">
                            <h6 className="text-white">Name</h6>
                            <span >{contactInfo.name}</span>
                        </div>
                    </li>
                    <li className="flex items-center">
                        <div className="pr-5">
                            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 384 512" fill="currentColor" width="32" height="32">
                                <path d="M168.3 499.2C116.1 435 0 279.4 0 192C0 85.96 85.96 0 192 0C298 0 384 85.96 384 192C384 279.4 267 435 215.7 499.2C203.4 514.5 180.6 514.5 168.3 499.2H168.3zM192 256C227.3 256 256 227.3 256 192C256 156.7 227.3 128 192 128C156.7 128 128 156.7 128 192C128 227.3 156.7 256 192 256z" />
                            </svg>
                        </div>
                        <div className="py-3 pl-6 border-l-2 border-white  border-opacity-20">
                            <h6 className="text-white">Location</h6>
                            <span >{contactInfo.location}</span>
                        </div>
                    </li>
                    <li className="flex items-center">
                        <div className="pr-5">
                            <svg xmlns="http://www.w3.org/2000/svg" width="32" height="32" fill="currentColor" className="bi bi-telephone-fill" viewBox="0 0 16 16">
                                <path fillRule="evenodd" d="M1.885.511a1.745 1.745 0 0 1 2.61.163L6.29 2.98c.329.423.445.974.315 1.494l-.547 2.19a.678.678 0 0 0 .178.643l2.457 2.457a.678.678 0 0 0 .644.178l2.189-.547a1.745 1.745 0 0 1 1.494.315l2.306 1.794c.829.645.905 1.87.163 2.611l-1.034 1.034c-.74.74-1.846 1.065-2.877.702a18.634 18.634 0 0 1-7.01-4.42 18.634 18.634 0 0 1-4.42-7.009c-.362-1.03-.037-2.137.703-2.877L1.885.511z" /> </svg>
                        </div>
                        <div className="py-3 pl-6 border-l-2 border-white  border-opacity-20">
                            <h6 className="text-white">Call Me</h6>
                            <a href="tel:{contactInfo.phoneNumber}" target="_blank">{contactInfo.phoneNumber}</a>
                        </div>
                    </li>
                    <li className="flex items-center">
                        <div className="pr-5">
                            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512" width="32" height="32" fill="currentColor" >
                                <path d="M511.6 36.86l-64 415.1c-1.5 9.734-7.375 18.22-15.97 23.05c-4.844 2.719-10.27 4.097-15.68 4.097c-4.188 0-8.319-.8154-12.29-2.472l-122.6-51.1l-50.86 76.29C226.3 508.5 219.8 512 212.8 512C201.3 512 192 502.7 192 491.2v-96.18c0-7.115 2.372-14.03 6.742-19.64L416 96l-293.7 264.3L19.69 317.5C8.438 312.8 .8125 302.2 .0625 289.1s5.469-23.72 16.06-29.77l448-255.1c10.69-6.109 23.88-5.547 34 1.406S513.5 32.72 511.6 36.86z" /></svg>
                        </div>
                        <div className="py-3 pl-6 border-l-2 border-white  border-opacity-20">
                            <h6 className="text-white">Email Me</h6>
                            <a href={`mailto:${contactInfo.email}`} target="_blank">{contactInfo.email}</a>
                        </div>
                    </li>
                </ul>
            </div>
        </div>

    )
}