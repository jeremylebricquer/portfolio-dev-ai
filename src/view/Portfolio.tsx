import { useState } from 'react';
import { Carousel } from '../components/Carousel';
import { projects } from '../data/projects'

export const Portfolio = () => {

    const [isOpen, setIsOpen] = useState(false);
    const [selectedType, setSelectedType] = useState<string | null>(null);


    const types = [
        { label: 'All', value: null },
        { label: 'AI', value: 'AI' },
        { label: 'Web', value: 'Web' },
        { label: 'Machine Learning', value: 'Machine Learning' },
        { label: 'Data science', value: 'Data science' },
    ];

    const toggleDropdown = () => {
        setIsOpen(!isOpen);
    };

    const handleTypeSelection = (type: string | null) => {
        setSelectedType(type);
        setIsOpen(false);
    };


    return (

        <div className="mx-auto w-11/12 max-w-7xl 2xl:w-4/5">
            <div className="items-center justify-center">
                <div className="relative min-w-[20rem] text-white mx-auto w-52 flex justify-center">
                    <button
                        className="flex w-[70vw] xl:w-full items-center justify-between rounded-sm border border-neutrals-800 bg-neutrals-800 px-4 py-2 text-sm text-neutrals-100 "
                        type="button"
                        aria-haspopup="listbox"
                        aria-expanded={isOpen}
                        onClick={toggleDropdown}>
                        {selectedType ? selectedType : 'All'}
                        <svg
                            xmlns="http://www.w3.org/2000/svg"
                            viewBox="0 0 24 24"
                            fill="currentColor"
                            aria-hidden="true"
                            className="h-4 w-4">
                            <path
                                fillRule="evenodd"
                                d="M12.53 16.28a.75.75 0 01-1.06 0l-7.5-7.5a.75.75 0 011.06-1.06L12 14.69l6.97-6.97a.75.75 0 111.06 1.06l-7.5 7.5z"
                                clipRule="evenodd">
                            </path>
                        </svg>
                    </button>
                    {isOpen && (
                        <ul className="w-52 absolute z-10  bg-black/40 py-1 mt-1 rounded-md shadow-lg border-solid border-2 border-neutrals-800 backdrop-blur-sm ">
                            {types.map((type) => (
                                <li key={type.value} className="hover:bg-green-500">
                                    <button
                                        onClick={() => handleTypeSelection(type.value)}
                                        className={`w-full text-left px-4 py-2 text-sm ${selectedType === type.value ? 'font-bold' : ''}`}>
                                        {type.label}
                                    </button>
                                </li>
                            ))}
                        </ul>
                    )}
                </div>
            </div>
            <Carousel selectedType={selectedType} projects={projects} />
        </div>

    )
}