import Writer from '../components/Writer'

export const Home = () => {

    const text = ["Developer", "Tech and AI Enthousiast"]


    return (
        <section className="h-full overflow-hidden">
            <div className="container mx-auto px-3 h-full">
                <div className="flex flex-row h-full items-center justify-center">
                    <div className="flex flex-col text-center">
                        <h1 className="text-white md:mb-8 font-bold text-5xl lg:text-7xl mt-40 md:mt-0" style={{ letterSpacing: '1px' }}>Jérémy <span >Le bricquer</span></h1>
                        <span className="text-2xl font-medium text-white block">
                            <Writer superpowers={text} />
                        </span>
                    </div>
                </div>
                <div className="relative mt-auto text-gray-200 text-center">
                    <p className="absolute inset-x-0 bottom-0 ">I'm currently looking for a job in AI and data<span className="block">Don't hesitate to contact me!</span></p>
                </div>
                <div className="absolute bottom-16 right-0 invisible md:visible">
                    <ul className="mb-0 mr-12">
                        <li className="mt-3 text-center" style={{ lineHeight: "100%" }}>
                            <a href="https://www.linkedin.com/in/jeremy-lebricquer/" target="_blank" aria-label="Linkedin link">
                                <svg style={{ color: "white" }} xmlns="http://www.w3.org/2000/svg" width="24" height="24" fill="currentColor" className="bi bi-linkedin" viewBox="0 0 16 16">
                                    <path d="M0 1.146C0 .513.526 0 1.175 0h13.65C15.474 0 16 .513 16 1.146v13.708c0 .633-.526 1.146-1.175 1.146H1.175C.526 16 0 15.487 0 14.854V1.146zm4.943 12.248V6.169H2.542v7.225h2.401zm-1.2-8.212c.837 0 1.358-.554 1.358-1.248-.015-.709-.52-1.248-1.342-1.248-.822 0-1.359.54-1.359 1.248 0 .694.521 1.248 1.327 1.248h.016zm4.908 8.212V9.359c0-.216.016-.432.08-.586.173-.431.568-.878 1.232-.878.869 0 1.216.662 1.216 1.634v3.865h2.401V9.25c0-2.22-1.184-3.252-2.764-3.252-1.274 0-1.845.7-2.165 1.193v.025h-.016a5.54 5.54 0 0 1 .016-.025V6.169h-2.4c.03.678 0 7.225 0 7.225h2.4z" fill="white"></path>
                                </svg>
                            </a>
                        </li>
                        <li className="mt-3 text-center" style={{ lineHeight: "100%" }}>
                            <a href="https://gitlab.com/jeremylebricquer" target="_blank" aria-label="gitlab link">
                                <svg style={{ color: "white" }} xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" strokeWidth="2" strokeLinecap="round" strokeLinejoin="round" className="feather feather-gitlab">
                                    <path d="M22.65 14.39L12 22.13 1.35 14.39a.84.84 0 0 1-.3-.94l1.22-3.78 2.44-7.51A.42.42 0 0 1 4.82 2a.43.43 0 0 1 .58 0 .42.42 0 0 1 .11.18l2.44 7.49h8.1l2.44-7.51A.42.42 0 0 1 18.6 2a.43.43 0 0 1 .58 0 .42.42 0 0 1 .11.18l2.44 7.51L23 13.45a.84.84 0 0 1-.35.94z" fill="white"></path></svg>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
        </section>
    )
}



