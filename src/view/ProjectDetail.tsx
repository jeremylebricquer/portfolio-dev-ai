import { useState } from "react";
import { ProjectDetailProps } from "../interface/projectDetail";

export const ProjectDetail: React.FC<ProjectDetailProps> = ({ selectedProject, isModalOpen, closeModal }) => {

    const [isClosing, setIsClosing] = useState(false);

    const handleCloseModal = () => {
        setIsClosing(true);
    };

    const handleTransitionEnd = () => {
        if (isClosing) {
            setIsClosing(false);
            closeModal();
        }
    };
    return (
        <>
            {isModalOpen && selectedProject && (
                <div
                    className={`fixed inset-0 flex justify-center bg-black z-10 overflow-auto transition-opacity duration-300 ${isClosing ? 'opacity-0' : ''}`}
                    role="dialog"
                    onTransitionEnd={handleTransitionEnd}
                >
                    <button className="absolute top-4 right-4">
                        <span className="text-white text-6xl leading-none inline-block mr-3 hover:animate-spin ease duration-300" onClick={handleCloseModal}>
                            ×
                        </span>
                    </button>
                    <div className="lg:container md:mx-auto mt-16 h-3/4 lg:overflow-y-hidden">
                        <div className="flex-auto flex-row">
                            <div className="flex-col px-4">
                                <div className="flex-row flex-1 mb-10">
                                    <div className="mb-7 text-center">
                                        <h2 className="text-4xl font-bold text-white mb-4 xl:text-5xl">{selectedProject.title}</h2>
                                        <p className="mb-3">{selectedProject.description}</p>
                                    </div>
                                    <div className="flex flex-col md:flex-row items-center h-[75vh]">
                                        <div className="w-2/3 pr-8 flex justify-center">
                                            <img src={selectedProject.image} alt={selectedProject.description} className="max-w-full h-auto rounded-[1%]" />
                                        </div>
                                        <div className="md:w-1/3 mt-5 md:mt-0">
                                            <div className="flex flex-wrap whitespace-break-spaces mb-4">
                                                <span className="mr-4">Tags:</span>
                                                {selectedProject.tags.map((value, index) => (
                                                    <ul key={index}>
                                                        <li>{value}</li>
                                                    </ul>
                                                ))}
                                            </div>
                                            <div>
                                                <a href={selectedProject.link} target="_blank" rel="noopener noreferrer" className="text-white underline hover:no-underline">
                                                    View Project
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            )}
        </>
    );
};
