
import photo_rounded from '../assets/img/photo_cv_rounded_bg.webp'
import cv from '../assets/doc/CV_Jérémy_Le bricquer.pdf'
import { about } from '../data/about'

export const About = () => {

    return (

        <div className="grid grid-cols-1 lg:grid-cols-2 gap-8 pb-6 text-lg items-center md:pt-8">
            <div className="flex justify-center">
                <img src={photo_rounded} alt="photo_rounded" className="bg-transparent border-black/20 w-60 lg:w-1/2" />
            </div>
            <div>
                <div className="">
                    <h2 className="text-lg mb-4 text-white">Who am i?</h2>
                    <h6 className="text-2xl text-white mb-6 font-bold">{about.title}</h6>
                    <div className="text-sm pb-6 mb-0 border-b border-white/70">
                        <p style={{ lineHeight: "1.95" }}>{about.desc}</p>
                    </div>

                    <div className="grid grid-cols-1 lg:grid-cols-2 lg:gap-4 gap-1 pb-6 text-sm my-6">
                        <div className="md:flex md:flex-col">
                            <div className="md:flex md:items-center mb-4">
                                <span className=" text-white/80 mr-2">Name:</span>
                                <p className="inline-block whitespace-nowrap">{about.name}</p>
                            </div>
                            <div className="md:flex md:items-center mb-4">
                                <span className=" text-white/80 mr-2">Age:</span>
                                <p className="inline-block whitespace-nowrap">{about.age}</p>
                            </div>
                        </div>
                        <div className="md:flex md:flex-col">
                            <div className="md:flex md:items-center mb-4">
                                <span className=" text-white/80 mr-2">Email:</span>
                                <p className="inline-block whitespace-nowrap">
                                    <a href={`mailto:${about.email}`} target="_blank">{about.email} </a>
                                </p>
                            </div>
                            <div className="md:flex md:items-center mb-4">
                                <span className=" text-white/80 mr-2">From:</span>
                                <p className="inline-block whitespace-nowrap">{about.from}</p>
                            </div>
                        </div>
                    </div>


                    <div className="flex items-center space-x-4 sm:space-x-6 md:space-x-8">
                        <a className="bg-white text-sm py-2 px-8 border-2 border-white rounded-3xl text-gray-700 whitespace-nowrap transition-opacity hover:opacity-75" href={cv} target="_blank" role="button">Download CV</a>
                        <div className="w-full bg-white mx-0 my-3" style={{ height: "1px" }}></div>
                        <ul className="flex flex-row space-x-4 sm:space-x-6">
                            <li className="text-center mr-4" style={{ lineHeight: "100%" }}>
                                <a href={about.linkedin} target="_blank">
                                    <svg style={{ color: "white" }} xmlns="http://www.w3.org/2000/svg" width="24" height="24" fill="currentColor" className="bi bi-linkedin" viewBox="0 0 16 16">
                                        <path d="M0 1.146C0 .513.526 0 1.175 0h13.65C15.474 0 16 .513 16 1.146v13.708c0 .633-.526 1.146-1.175 1.146H1.175C.526 16 0 15.487 0 14.854V1.146zm4.943 12.248V6.169H2.542v7.225h2.401zm-1.2-8.212c.837 0 1.358-.554 1.358-1.248-.015-.709-.52-1.248-1.342-1.248-.822 0-1.359.54-1.359 1.248 0 .694.521 1.248 1.327 1.248h.016zm4.908 8.212V9.359c0-.216.016-.432.08-.586.173-.431.568-.878 1.232-.878.869 0 1.216.662 1.216 1.634v3.865h2.401V9.25c0-2.22-1.184-3.252-2.764-3.252-1.274 0-1.845.7-2.165 1.193v.025h-.016a5.54 5.54 0 0 1 .016-.025V6.169h-2.4c.03.678 0 7.225 0 7.225h2.4z" fill="white"></path>
                                    </svg>
                                </a>
                            </li>
                            <li className="text-center mr-4" style={{ lineHeight: "100%" }}>
                                <a href={about.gitlab} target="_blank">
                                    <svg style={{ color: "white" }} xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" strokeWidth="2" strokeLinecap="round" strokeLinejoin="round" className="feather feather-gitlab">
                                        <path d="M22.65 14.39L12 22.13 1.35 14.39a.84.84 0 0 1-.3-.94l1.22-3.78 2.44-7.51A.42.42 0 0 1 4.82 2a.43.43 0 0 1 .58 0 .42.42 0 0 1 .11.18l2.44 7.49h8.1l2.44-7.51A.42.42 0 0 1 18.6 2a.43.43 0 0 1 .58 0 .42.42 0 0 1 .11.18l2.44 7.51L23 13.45a.84.84 0 0 1-.35.94z" fill="white"></path></svg>
                                </a>
                            </li>
                        </ul>
                    </div>

                </div>
            </div>
        </div >

    );
}