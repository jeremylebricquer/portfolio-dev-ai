import { skillList } from "../data/skills"
import { softSkillList } from "../data/skills"

export const Skills = () => {

    return (

        <div className="container mx-auto mt-6">
            <div className="mb-8">
                <div className="mb-12">
                    <p className="text-base mb-4">My level of knowledge in some tools</p>
                    <h2 className="text-4xl">My Hard Skills</h2>
                </div>

                <div className="grid grid-cols-1 md:grid-cols-2 gap-8 pb-6 text-lg">
                    {skillList.map((value, key) =>
                        <div key={key} >
                            <div className="h-4 relative w-60 rounded-full overflow-hidden mb-2">
                                <div className="w-full h-full bg-gray-200 absolute"></div>
                                <div className="h-full bg-yellow-400 sm:bg-green-500 absolute" style={{ width: `${parseInt(value.percent)}%` }}></div>
                                <div className="absolute inset-0 flex items-center justify-center text-xs text-white"> {value.percent}</div>
                            </div>
                            <h1 className="text-white ">{value.type} : {value.skills}</h1>
                        </div>
                    )}
                </div>
            </div>
            <div className="mb-8">
                <div className="mb-12">
                    <p className="text-base mb-4">My everyday skills</p>
                    <h2 className="text-4xl">My Soft Skills</h2>
                </div>

                <div className="grid grid-cols-1 md:grid-cols-2 gap-8 pb-6 text-lg">
                    {softSkillList.map((value, key) =>
                        <div key={key}>
                            <div className="h-4 relative w-60 rounded-full overflow-hidden mb-2">
                                <div className="w-full h-full bg-gray-200 absolute"></div>
                                <div className="h-full bg-yellow-400 sm:bg-green-500 absolute" style={{ width: `${parseInt(value.percent)}%` }}></div>
                                <div className="absolute inset-0 flex items-center justify-center text-xs text-white"> {value.percent}</div>
                            </div>
                            <h1 className="text-white ">{value.type}</h1>
                        </div>
                    )}
                </div>
            </div>
        </div>
    )
}