import { referenceList } from "../data/references";
import { resumeInfo } from "../data/resume"
import { resumeType } from "../data/resume";
import { Reference } from "./Reference";
import { Skills } from "./Skills";

export const Resume = () => {

    return (
        <div className="container mx-auto">
            <div className="grid md:grid-cols-2 sm:grid-cols-1 gap-4 mb-20">
                {Object.keys(resumeInfo).map((key) => {
                    if (Object.keys(resumeInfo).includes(key)) {
                        return (
                            <div key={key} className="mb-3">
                                <h2 className="text-2xl font-bold mb-4">{key}</h2>
                                {resumeInfo[key as keyof resumeType].map((item, index) => (
                                    <div className="py-8 px-12 border-b border-white/40 bg-gray-800/50 rounded-t-lg" key={index}>
                                        <h4 className="text-lg font-semibold text-white mb-2">{Object.keys(item)[0]}</h4>
                                        <p className="text-md  font-medium  italic mb-2">{Object.values(item)[0][0]}</p>
                                        <p className="text-md font-medium" style={{ lineHeight: '1.95' }}>{Object.values(item)[0][1]}</p>
                                        {key === 'Experience' &&
                                            <ul>
                                                <li> <p className="text-md font-medium" style={{ lineHeight: '1.95' }}>{Object.values(item)[0][2]}</p></li>
                                                {Object.values(item)[0][3] != null &&
                                                    <li> <a href={Object.values(item)[0][3]} target="_blank" className="text-md font-medium" style={{ lineHeight: '1.95' }}>Check out this repo!</a></li>}
                                            </ul>
                                        }
                                    </div>
                                ))}
                            </div>
                        );
                    }
                    return null;
                })}
            </div>
            < Skills />
            <Reference references={referenceList} />
        </div>
    );
}