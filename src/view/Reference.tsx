import { ReferenceList } from "../data/references";

interface ReferenceListProps {
    references: ReferenceList[];
}

export const Reference: React.FC<ReferenceListProps> = ({ references }) => {
    return (
        <div className="container mx-auto mt-6">
            <div className="mb-8">
                <div className="mb-12">
                    <p className="text-base mb-4">Tutor's Recommendations</p>
                    <h2 className="text-4xl">References</h2>
                </div>
                <div className="pb-20">
                    {references.map((reference, index) => (
                        <div key={index} className="bg-gray-800/50 rounded-lg px-6 pt-14 pb-4 mb-4 relative">
                            <span className="testimonials text-[180px] leading-none absolute -top-2 left-8 text-white">"</span>
                            <p className="text-gray-300 leading-[1.75] py-4">
                                {reference.text}</p>
                            <h3 className="text-lg font-semibold text-white">{reference.title}</h3>
                        </div>
                    ))}
                </div>
            </div>
        </div >
    )
}

