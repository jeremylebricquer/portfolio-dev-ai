import { useState, useRef } from "react";

import { Projects } from "../interface/Projects";
import { ProjectDetail } from "../view/ProjectDetail";

type CarouselProps = {
    selectedType: string | null;
    projects: Projects[]
};

export const Carousel = ({ selectedType, projects }: CarouselProps) => {
    const [startX, setStartX] = useState<number | null>(null);
    const [scrollLeft, setScrollLeft] = useState<number | null>(null);
    const carouselRef = useRef<HTMLDivElement>(null);
    const [selectedProject, setSelectedProject] = useState<Projects | null>(null);
    const [isModalOpen, setIsModalOpen] = useState(false);

    const closeModal = () => {
        setIsModalOpen(false)
    }

    const handleMouseDown = (event: React.MouseEvent<HTMLDivElement>) => {
        const currentRef = carouselRef.current;
        if (currentRef) {
            setStartX(event.pageX - currentRef.offsetLeft);
            setScrollLeft(currentRef.scrollLeft);
            currentRef.style.cursor = "grabbing";
        }
    };

    const handleMouseMove = (event: React.MouseEvent<HTMLDivElement>) => {
        const currentRef = carouselRef.current;
        if (!startX || !currentRef) return;
        event.preventDefault();
        const x = event.pageX - currentRef.offsetLeft;
        const walk = (x - startX) * 1.5; // Adjust the scrolling speed here
        currentRef.scrollLeft = (scrollLeft ?? 0) - walk;
    };

    const handleMouseUp = () => {
        const currentRef = carouselRef.current;
        if (currentRef) {
            setStartX(null);
            currentRef.style.cursor = "grab";
        }
    };

    const handleCardClick = (project: Projects) => {
        setSelectedProject(project);
        setIsModalOpen(true);
    };


    const filteredProjects = selectedType ? projects.filter((x) => x.type === selectedType) : projects;

    return (
        <div className='container mx-auto py-8 min-w-[75vw] md:min-w-full w-[75vw]'>

            <div
                ref={carouselRef}
                className="overflow-x-scroll whitespace-nowrap pb-5 scrollbar"
                onMouseDown={handleMouseDown}
                onMouseMove={handleMouseMove}
                onMouseUp={handleMouseUp}
                onMouseLeave={handleMouseUp}
                style={{ scrollBehavior: "smooth", scrollSnapType: "x mandatory", WebkitOverflowScrolling: "touch", }}>

                {filteredProjects.map((project) => (
                    <div key={project.id} className="inline-block mr-4 group group-hover:opacity-100 group-focus-visible:opacity-100" onClick={() => handleCardClick(project)}>
                        <div className="flex bg-white rounded-lg shadow-lg">
                            <div className="inline-flex gap-[3vmin]">
                                <div className="relative aspect-[2/3] h-[max(55vmin,20rem)] rounded-md overflow-hidden" style={{ backgroundColor: "rgb(52, 69, 92)", opacity: "1", transform: "translateY(0%) translateZ(0px)" }} key={project.id}>
                                    <div className="absolute inset-0 flex flex-col items-center justify-center bg-black/50 p-4 text-center opacity-0 backdrop-blur-sm transition-all duration-300 group-hover:opacity-100 group-focus-visible:opacity-100 gap-y-2">
                                        <div className="overflow-hidden">
                                            <h3 className="translate-y-full text-2xl font-bold transition-transform duration-300 group-hover:translate-y-0 group-focus-visible:translate-y-0 text-green-500 whitespace-break-spaces">{project.title}</h3>
                                        </div>
                                        <div className="overflow-hidden">
                                            <p className="translate-y-full text-sm lg:text-base transition-transform duration-300 group-hover:translate-y-0 group-focus-visible:translate-y-0 text-white whitespace-break-spaces">{project.description}</p>
                                        </div>
                                    </div>
                                    <img alt="" loading="lazy" decoding="async" src={project.image} className="pointer-events-none -z-10 h-full w-full object-cover absolute inset-0 group-hover:scale-105 group-focus-visible:scale-105 transition-transform duration-700 object-left " />
                                </div>
                            </div>
                        </div>
                    </div>
                ))}
            </div>
            <ProjectDetail isModalOpen={isModalOpen} selectedProject={selectedProject} closeModal={closeModal} />
        </div >
    );
}