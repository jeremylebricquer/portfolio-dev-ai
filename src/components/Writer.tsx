import { useTypedSuperpower } from './TypeWriter'

type Props = {
    superpowers: string[]
}

const Writer = ({ superpowers }: Props) => {
    const { typedSuperpower, selectedSuperpower, resume } =
        useTypedSuperpower(superpowers)

    return (
        <h2
            className="flex flex-col lg:block text-center pt-12 tracking-tight "
            onClick={resume}
        >
            <span className="mb-2 lg:mb-0"> I’m a</span>{' '}
            <span
                className=' pt-4 pb-12 min-h-[10rem]'
                aria-label={selectedSuperpower}
            >
                {typedSuperpower}
            </span>
        </h2>
    )
}

export default Writer