import { useEffect, useState } from "react";
import { NavbarProps } from "../interface/navbar";

export const Navbar: React.FC<NavbarProps> = ({ links, openModal }) => {

    const [isNavbarOpen, setIsNavbarOpen] = useState(false);


    const toggleNavbar = () => {
        setIsNavbarOpen(!isNavbarOpen);
    };

    const closeNavbar = () => {
        setIsNavbarOpen(false);
    };

    useEffect(() => {
        const handleResize = () => {
            if (window.innerWidth > 768) {
                closeNavbar();
            }
        };

        window.addEventListener("resize", handleResize);

        return () => {
            window.removeEventListener("resize", handleResize);
        };
    }, []);

    return (

        <nav className="fixed top-0 left-0 right-0h-16 flex items-center justify-between w-full px-12 py-5">
            <ul className="hidden space-x-2 md:inline-flex ml-auto">
                {links.map((link) => (
                    <li key={link.id}>
                        <button className="px-4 py-2 font-medium text-white rounded text-lg" onClick={() => openModal(link)} aria-label="Menu de navigation" role="button">
                            {link.text}
                        </button>
                    </li>
                ))}
            </ul>
            <button
                className="text-white focus:outline-none md:hidden ml-auto"
                onClick={toggleNavbar}
                aria-label="Menu de navigation"
                title="menu"
                role="navigation"
            >
                <svg xmlns="http://www.w3.org/2000/svg" className="w-8 h-8 -rotate-180" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                    <path strokeLinecap="round" strokeLinejoin="round" strokeWidth="2" d="M4 6h16M4 12h8m-8 6h16" />
                </svg>
            </button>


            {
                isNavbarOpen && (
                    <div
                        className="absolute top-14 right-12 shadow-md py-2 px-5 "
                        style={{
                            backgroundColor: "#191919",
                            width: "200px",
                        }}
                    >
                        <ul className="flex flex-col text-left text-white/70 ml-12">
                            {links.map((link) => (
                                <li className="py-1" key={link.id}>
                                    <button className="py-1 text-sm " style={{ letterSpacing: "0.35px" }} onClick={() => openModal(link)}>
                                        {link.text}
                                    </button>
                                </li>
                            ))}
                        </ul>
                    </div>
                )
            }
        </nav >


    )
}