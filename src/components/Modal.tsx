import { useState } from "react";
import { ModalProps } from "../interface/modal";

export const Modal: React.FC<ModalProps> = ({ isOpen, closeModal, title, desc, children }) => {

    const [isClosing, setIsClosing] = useState(false);

    const handleCloseModal = () => {
        setIsClosing(true);
    };

    const handleTransitionEnd = () => {
        if (isClosing) {
            setIsClosing(false);
            closeModal();
        }
    };
    return (
        <>
            {
                isOpen ? (
                    <div
                        className={`fixed inset-0 flex justify-center bg-black z-10 overflow-auto transition-opacity duration-500 ${isClosing ? 'opacity-0' : ''}`}
                        role="dialog"
                        onTransitionEnd={handleTransitionEnd}
                    >
                        <button className="absolute top-4 right-4" onClick={handleCloseModal}>
                            <span className="text-white text-6xl leading-none inline-block mr-3 hover:animate-spin ease duration-300">×</span>
                        </button>
                        <div className="lg:container md:mx-auto mt-16">
                            <div className="flex-auto flex-row">
                                <div className="flex-col px-4">
                                    <div className="flex-row flex-1 mb-20">
                                        <div className="mb-7 text-center">
                                            <p className="mb-3">{desc}</p>
                                            <h2 className="text-4xl font-bold text-white mb-4 xl:text-5xl">{title}</h2>
                                        </div>
                                        <div className="loader">
                                            <div className="loaderBar"></div>
                                        </div>
                                    </div>
                                    {children}
                                </div>
                            </div>
                        </div>

                    </div>

                ) : null
            }
        </>
    );
}